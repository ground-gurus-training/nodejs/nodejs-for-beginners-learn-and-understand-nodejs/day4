import "./style.css";
import axios from "axios";

init();

function init() {
  getRandomDadJoke();

  document.querySelector("#btn").addEventListener("click", () => {
    getRandomDadJoke();
  });
}

function getRandomDadJoke() {
  axios
    .get("https://icanhazdadjoke.com/", {
      headers: {
        Accept: "text/plain",
      },
    })
    .then(function (response) {
      document.querySelector("#app").innerHTML = `${response.data}`;
    })
    .catch(function (error) {
      console.log(error);
    });
}
